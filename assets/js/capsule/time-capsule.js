function getNewOrbit(i, spacing) {
    return $('<div class="time-capsule-orbit"></div>').css({
        'width':  ((i / 1.5) * spacing) + 150,
        'height': ((i / 1.5) * spacing) + 150,
    });
}

function getCapsule(capsuleName, data) {
    var spacing = 90;
    var $wrapper = $('<div></div>');
    var $orbitContent = getNewOrbit(0, spacing);
    $wrapper.append($orbitContent);

    $.each(data, function(i, value) {
        var content = value.type === 'text' ? btoa(value.content) : value.content;
        var description = typeof value.description !== 'undefined' ? value.description : '';

        var dataSet = 'data-capsule-name="' + capsuleName + '" data-type="' + value.type + '" data-content="' + content + '"' + 'data-description="' + description + '"';

        if (i > 1 && (i % 2 === 0)) {
            $orbitContent = getNewOrbit(i, spacing);
            $wrapper.append($orbitContent);
        }

        var $capsuleContent = $('<div class="time-capsule-content" ' + dataSet + '></div>').appendTo($orbitContent);
    });

    return $wrapper.html();
}

$(function() {
    var aladin = A.aladin('#aladin-lite-div', { 
        fov: 1.2,

        survey: "P/DSS2/color", 
        target: "SDSS J151602.89+000519.4",
        showReticle: false,
        showFullscreenControl: false,
        showLayersControl: false,
        showGotoControl: false,
        showFrame: false,
        fullScreen: true
    });

    var $container = $('#time-capsule-container');

    $.getJSON('http://localhost:8080/database.json', function (timeCapsules) {
        var openCapsulesCat = A.catalog({
            sourceSize: 16,
            color: 'green',
        });

        var closedCapsulesCat = A.catalog({
            sourceSize: 16,
            color: 'red',
        });
        
        aladin.addCatalog(openCapsulesCat);
        aladin.addCatalog(closedCapsulesCat);

        $.each(timeCapsules || [], function(key, capsule) {
            if (capsule.isOpen) {
                openCapsulesCat.addSources([
                    // position[0] = RA, position[1] = DEC, "DEC RA" = J2000
                    A.marker(capsule.position[0], capsule.position[1], {
                        popupTitle: capsule.userName,
                        popupDesc: getCapsule(capsule.capsuleName, capsule.data)
                    })
                ]);
            } else {
                closedCapsulesCat.addSources([
                    // position[0] = RA, position[1] = DEC, "DEC RA" = J2000
                    A.marker(capsule.position[0], capsule.position[1], {
                        popupTitle: capsule.userName,
                        popupDesc: "Cápsula fechada, será aberta daqui " + capsule.years + " anos"
                    })
                ]);
            }
        });
    });

    var clickInterval;
    var lastPopupData;
    var isDragging = false;
    var $popupContainer = $('.aladin-popup-container');

    $('.aladin-reticleCanvas').on("click touchend", function() {
        if (lastPopupData && lastPopupData != $popupContainer.text()) {
            $popupContainer.removeClass('visible');
        }

        clickInterval = setInterval(function() {
            if (lastPopupData && lastPopupData != $popupContainer.text()) {
                clearInterval(clickInterval);
                lastPopupData = $popupContainer.text();

                setTimeout(function() {
                    $popupContainer.addClass('visible');
                }, 200);

            } else if ($popupContainer.is(':visible')) {
                clearInterval(clickInterval);
                $popupContainer.addClass('visible');
                lastPopupData = $popupContainer.text();

            } else {
                $popupContainer.removeClass('visible');
            }
        }, 100);

        setTimeout(function() {
            clearInterval(clickInterval);
        }, 1000);
    }).mousedown(function() {
        isDragging = false;

    }).mousemove(function() {
        isDragging = true;

    }).mouseup(function() {
        var wasDragging = isDragging;
        isDragging = false;
        if (wasDragging) {
            $('.aladin-closeBtn').click();
        }
    });

    $('.aladin-closeBtn').on("click touchend", function() {
        $popupContainer.removeClass('visible');
    });
});