$(document).on('click', '.time-capsule-content', function(){
    var dataType = $(this).attr('data-type');
    var dataContent = $(this).attr('data-content');
    var capsuleName = $(this).attr('data-capsule-name');
    var description = $(this).attr('data-description')
    var renderTemplate = '';

    switch(dataType) {
        case "video":
            renderTemplate = '<video class="video" src="' + dataContent + '" controls autoplay></video>';
            break;
        case "image":
            renderTemplate = '<img class="image" src="' + dataContent + '">';
            break;
        case "audio":
            renderTemplate = '<audio class="audio" controls>' +
                '<source src="' + dataContent + '" type="audio/mpeg">' +
                '</audio>';
            break;
        case "text":
            renderTemplate = '<div class="text">' + atob(dataContent)+ '</div>';
            break;
    }

    if(description != '') {
        renderTemplate = renderTemplate + '<p></p><p>' + description + '</p>';
    }

    $('.capsule-title').html(capsuleName);
    $('#detail-rendering-modal').modal('show');
    $("#loader-gif").show();
    $(".detail-content").html(renderTemplate).promise().done(function(){
        $("#loader-gif").hide();
    });
});

$(document).on('hidden.bs.modal', '#detail-rendering-modal', function(){
    $(".detail-content").children().each(function(){
        delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
        $(this).remove(); // this is probably what actually does the trick
    });
    $(".detail-content").empty();
});