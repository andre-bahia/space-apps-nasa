'use strict';

app.controller("usuarioCtrl", ['$scope', '$rootScope', '$timeout', '$http', function($scope, $rootScope, $timeout, $http){
	$scope.titulo = "Alterar Usuário";
	$scope.tituloBtnSubmit = "Salvar";
	// console.log(auth_user);
	// console.log($scope.auth_user);
	$timeout(function(){
		//não dá tempo do mainCtrl carregar, por isso o time out
		$rootScope.userProfile.patch = 'users/' + $rootScope.auth_user.id;
		$rootScope.userProfile.table = 'users';
		$rootScope.userProfile.active = '1';
		$rootScope.userProfile.order = '1';
		$rootScope.userProfile.table_id = $rootScope.auth_user.id;
		
	}, 300);


	// console.log($rootScope.userProfile);
	// $scope.cadastrar = function(){
	// 	// alert('a');
	// 	 console.log($rootScope.auth_user);

	// }
	// console.log($rootScope.userProfile);
	$scope.submit = function(dataUser){
		// alert('a');
		console.log("dataUser");
		console.log(dataUser);
		$http.put(BASE_URL + 'user/save/' + dataUser.id, dataUser).success(function(user){
			//Insere ou atualiza a imagem do usuário
			if($rootScope.userProfile.url.toString() != 'NaN'){
				// $rootScope.userProfile.url = $rootScope.auth_user.avatar.url;	
				
				$http.put(BASE_URL + 'images/save', $rootScope.userProfile).success(function(userProfile){
					console.log("userProfile");
					console.log(userProfile);
					$rootScope.userProfile = userProfile;
					$rootScope.auth_user.avatar.patch = userProfile.patch;
					$rootScope.auth_user.avatar.url = userProfile.url;
				}).error(function(e){
					console.log(e);
				});
			}else{
				$rootScope.userProfile.url = $rootScope.auth_user.avatar.url;
			}
		}).error(function(e){
			console.log(e);
		});
		// console.log($rootScope.auth_user);

	}

}]);
