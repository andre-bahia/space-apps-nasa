'use strict';
var app = angular.module("app", ['ngRoute', 'ngMask', 'capsula']).config(function ($routeProvider, $httpProvider) {

      $routeProvider
        .when('/', {
            templateUrl: 'dashboard/dashboard.html',
            controller: 'dashboardCtrl'
        })
        .when('/capsulas', {
            templateUrl: 'capsula/views/capsula.html',
            controller: 'capsulaCtrl',
        })
        .when('/capsula/cadastrar', {
            templateUrl: 'capsula/views/capsula-form.html',
            controller: 'capsulaCtrl',
        })
        .when('/capsula/:id/editar', {
            templateUrl: 'capsula/views/capsula-form.html',
            controller: 'capsulaCtrl',
            method: 'editar'
        })
        .when('/capsula/:id/visualizar', {
            templateUrl: 'capsula/views/capsula-view.html',
            controller: 'capsulaViewCtrl',
        })
        .otherwise({
            redirectTo: '/'
        });

  });
