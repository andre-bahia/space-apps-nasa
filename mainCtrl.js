'use strict';

app.controller('mainCtrl', ['$scope', '$rootScope', '$http', '$location', '$route', '$timeout', function ($scope, $rootScope, $http, $location, $route, Upload, $timeout) {

  $rootScope.maskTel = "(99) 9?9999-9999";
  $rootScope.maskCel = "(99) 9?9999-9999";
  $rootScope.maskCEP = "99999-999";
  $rootScope.maskCPF = "999.999.999-99";
  $rootScope.maskCNPJ = "99.999.999/9999-99";

  $rootScope.auth_user = {};
  $rootScope.userProfile = {};
  $rootScope.dataToTable = {};

  $rootScope.uploadFiles = function(file, errFiles, dataToTable) {
    // console.log(dataToTable);
    if(file == null){
      return false;
    }
    $rootScope.f = file;
    $rootScope.errFile = errFiles && errFiles[0];

    var dataFile = {'image': file, 'patch':''};//, 'table':'', 'table_id':'', 'order':'', 'active':''};

    if(typeof dataToTable.patch != 'undefined'){
      dataFile.patch = dataToTable.patch;
    }

    if (file) {
      file.upload = Upload.upload({
        url: '/images/save',
        data: dataFile
      });

      file.upload.then(function (response) {
        console.log(response.data);
         // file.result = response.data;
         // console.log($rootScope.dataToTable.url);
        $timeout(function (){
          file.result = response.data;
          if(file.result!= 'NaN'){
              dataToTable.url = file.result;
          }
        });
      }, function (response) {
        if (response.status > 0)
        $rootScope.errorMsg = response.status + ': ' + response.data;
        console.log($rootScope.errorMsg);
      }, function (evt) {
        file.progress = Math.min(100, parseInt(100.0 *
          evt.loaded / evt.total));
        });
      }
    }


    $rootScope.isActive = function (viewLocation) {

      return viewLocation === $location.path().split("/")[1];

    };



  }]);
