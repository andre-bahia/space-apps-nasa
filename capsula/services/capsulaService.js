capsula.factory("capsulaService", capsulaService);

function capsulaService($http) {

    function getCapsules() {
       return $http.get('/database.json').then(function(response){
           return response;
       }); 
    }

    function getById(id){
        var data = [];
        var capsule = {};
        return $http.get('/database.json').then(function(response){
            data = response.data;
            for (var key in data) {
                if(data[key].id === id){
                    capsule = data[key];
                    return capsule;
                }
            }
        });
    }

    return {
        getCapsules: getCapsules,
        getById: getById
    };
}