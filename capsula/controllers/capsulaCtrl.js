capsula.controller("capsulaCtrl", capsulaCtrl);

function capsulaCtrl($scope, capsulaService) {
    $scope.capsules = [];
    console.log("Capsula controller");

    function listCapsules(){
       var promise = capsulaService.getCapsules();
       promise.then(onLoadCapsules);
       promise.catch(onLoadError);
    }

    function onLoadCapsules(data){
        $scope.capsules = data.data
    }

    function onLoadError(data){
       console.log("Deu merda: "+ data);
    }
    listCapsules();
}