capsula.controller("capsulaViewCtrl", capsulaViewCtrl);

function capsulaViewCtrl($scope, $routeParams, capsulaService){
    var id = $routeParams.id;
    $scope.capsula = {};
    $scope.dataCapsule = {};
    
    function getById(idCapsule){
        var promise = capsulaService.getById(id);
        promise.then(onLoadCapsule);
        promise.catch(onLoadError);    
    }

    function onLoadCapsule(response) {
        $scope.capsula = response;
        $scope.dataCapsule = countTypes($scope.capsula.data);
    }

    function countTypes(data){
        var countAudio = 0;
        var countVideo = 0;
        var countText = 0;
        var countImage = 0;
        for (var key in data) {
            if (data[key].type == "audio"){
                countAudio++;
            }else if(data[key].type === "video"){
                 countVideo++;   
            }else if(data[key].type == "text"){
                countText++;
            }else if(data[key].type == "image"){
                countImage++;
            }
        }
        return [
            {"label": "Áudio", "count": countAudio, "icon":"mdi-image-audiotrack"},
            {"label": "Vídeo", "count": countVideo, "icon":"mdi-image-movie-creation"},
            {"label": "Arquivos de Texto", "count": countText, "icon":"mdi-editor-attach-file"},
            {"label": "Images", "count": countImage, "icon":"mdi-image-camera-alt"},

        ];
    }

    function onLoadError(response){
        console.log(response);
    }

    getById(id);
}